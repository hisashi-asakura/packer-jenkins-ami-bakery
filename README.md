# Get Started with Packer Image Bakery Repo

This repo contains three components:
* [Packer to Build AWS AMIs](#packer)
* [Ansible to configure EC2](#ansible)
    * [Why Ansible](#why_ansible)
    * [Ansible Roles Structures](#ansible_roles_structures)
    * [Ansible with Packer](#ansible_with_packer)
    * [Ansible Playbook Anatomy](#ansible_playbook_anatomy)
* [Jenkinsfile to _declaratively_ set up a Jenkins Pipeline for baking AMIs](#jenkins)
    * [Why Jenkins](#why_jenkins)
    * [Jenkinsfile as IaC](#jenkinsfile_as_iac)
    * [How to access Jenkins](#how_to_access_jenkins)
    * [Jenkins Anatomy](#jenkins_anatomy)
* [How to Test](#how_to_test)
    * [Local Testing](#local_testing)
        * [Prerequisite](#prerequisite)
        * [Packer Local Testing](#packer_local_testing)
        * [Ansible Local Testing](#ansible_local_testing)
    * [Remote Testing (on Jenkins)](#remote_testing)

## 1. Packer scripts to build AWS AMIs <a name="packer"></a>
Packer scripts are organized by folders with names of AMIs.
Take an example for AMI called `amazon_linux_2_ami`, there are [amazon_linux_2_ami.json](packer/amazon_linux_2_ami/amazon_linux_2_ami.json) and [variables.json](packer/amazon_linux_2_ami/variables.json).
```
packer/
├── README.md
├── amazon_linux_2_ami
│   ├── amazon_linux_2_ami.json
│   └── variables.json
```
From the Packer script, `ansible` packer provisioner  is used to execute Ansible playbook ([ansible/amazon_linux_2_ami/playbook.yml](ansible/amazon_linux_2_ami/playbook.yml)) against an EC2 instance.
```
{
  "type": "ansible-local",
  "playbook_file": "../../ansible/{{user `ami_type`}}/playbook.yml",
  "playbook_dir": "../../ansible"
}
```

`{{user `ami_type`}}` is a packer variable that gets injected from [variables.json](packer/amazon_linux_2_ami/variables.json), which the Jenkins Pipeline (i.e. [Jenkinsfile](Jenkinsfile)) would modify:
```
def replaceVariables(baseAMI, sshUsername) {
    .
    .
    .
    sed -i 's|"aws_region.*|"aws_region": "'"${params.AWS_REGION}"'",|
            s|"base_ami.*|"base_ami": "'"${baseAMI}"'",|
            s|"instance_type.*|"instance_type": "'"${INSTANCE_TYPE}"'",|
            s|"name.*|"name": "'"${params.AMI_TYPE}"'",|
            s|"ssh_username.*|"ssh_username": "'"${sshUsername}"'",|
            s|"tag_name.*|"tag_name": "'"${TAG_NAME}"' '"${params.AMI_TYPE}"'",|
            s|"ami_type.*|"ami_type": "'"${params.AMI_TYPE}"'",|
            s|"kms_key_id.*|"kms_key_id": "'"${params.KMS_KEY}"'",|
            s|"aws_access_key.*|"aws_access_key": "'"\${AWS_ACCESS_KEY_ID}"'",|
            s|"aws_secret_key.*|"aws_secret_key": "'"\${AWS_SECRET_ACCESS_KEY}"'",|
}
```

## 2. Ansible Roles to Configure EC2 <a name="ansible"></a>

### Why Ansible <a name="why_ansible"></a>
Ansible is used to configure and install packages and file permissions etc. Similar to Packer folder, Ansible Playbook files are organized by folders with names of AMIs.
```
ansible/
├── README.md
├── amazon_linux_2_ami
├── rhel_7_ami
├── roles
│   ├── aws-inspector-agent

```

### Ansible Roles Structures <a name="ansible_roles_structures"></a>
_Common_ Ansible roles are organized in `ansible/roles` folders, and the path to the specific roles are specified from each playbook file.
Take an example for `amazon_linux_2_ami` again, there are [playbook.yml](ansible/amazon_linux_2_ami/playbook.yml):
```
---
- name: Converge
  hosts: all
  become: true

  roles:
    - { role: ../roles/aws-inspector-agent }
```

The line `{ role: ../roles/aws-inspector-agent }` is specifying a **_local_** file path to the Roles.

However, _AMI-specific_ Ansible roles are organized inside AMI's folder. If you look at `ubuntu_jenkins_ami`, there are [playbook.yml](ubuntu_jenkins_ami/playbook.yml), [requirements.yml](ubuntu_jenkins_ami/requirements.yml), and a few Ansible roles specific to the Ubuntu Jenkins AMI:
```
ansible/ubuntu_jenkins_ami/
├── ansible.cfg
├── aws-cli
├── jenkins-permission
├── nfs-util
├── playbook.yml
└── requirements.yml
```

[requirements.yml](ubuntu_jenkins_ami/requirements.yml) specifies names of _remote_ Ansible Roles to download from Ansible Galaxy:
```
---
- src: geerlingguy.java
- src: geerlingguy.jenkins
- src: geerlingguy.docker
- src: sansible.amazon_inspector_agent
- src: dharrisio.aws-cloudwatch-logs-agent
- src: dhoeric.aws-ssm
```

### Ansible with Packer <a name="ansible_with_packer"></a>

Note that this file needs to be copied to an EC2 instance (in [ubuntu_jenkins_ami.json](packer/ubuntu_jenkins_ami/ubuntu_jenkins_ami.json)). 
```
{
  "type": "file",
  "source": "../../ansible/{{user `ami_type`}}/requirements.yml",
  "destination": "/tmp/requirements.yml"
},
```
Then to download them to an EC2 instance, execute `sudo ansible-galaxy install -r /tmp/requirements.yml -p /etc/ansible/roles` in the last command in the below inline shell in [ubuntu_jenkins_ami.json](packer/ubuntu_jenkins_ami/ubuntu_jenkins_ami.json):
```
{
  "type": "shell",
  "inline": [
    "sudo apt-get update && sudo apt-add-repository ppa:ansible/ansible && sudo apt-get update && sudo apt-get install -y ansible && sudo ansible-galaxy install -r /tmp/requirements.yml -p /etc/ansible/roles"
  ]
}
```

### Ansible Playbook Anatomy <a name="ansible_playbook_anatomy"></a> 
Finally, [playbook.yml](ubuntu_jenkins_ami/playbook.yml) specifies both remote and local Ansible roles:
```
roles:
    - geerlingguy.java
    - geerlingguy.jenkins
    - geerlingguy.docker
    - nfs-util
    - jenkins-permission
    - aws-cli
    - { role: ../roles/aws-cloudwatch-logs-rhel }
    - { role: ../roles/aws-ssm-agent }
    - { role: ../roles/aws-inspector-agent }
```

## 3. Jenkinsfile to Set Up a Jenkins Pipeline <a name="jenkins"></a>

### Why Jenkins <a name="why_jenkins"></a>

Deployment of the Terraform code should be done from Jenkins CI/CD Pipeline to ensure continuous and autonomous build and delivery.

### Jenkinsfile as IaC <a name="jenkinsfile_as_iac"></a>

[Jenkinsfile](Jenkinsfile) at the root of this repo defines a Jenkins Pipeline in a declarative manner using Groovy language. This IaC approach eliminates manual setup of a Jenkins pipeline.

### How to access Jenkins <a name="how_to_access_jenkins"></a>
1. Jenkins infra resources should be already deployed to AWS by other repo [solvay-clz-shared-jenkins](https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/solvay-clz-shared-jenkins) as per prerequisite
2. create ssh tunnel from your local environment to the Jenkins EC2 through the bastion instance, if accessing the Jenkins from outside the Solvay network
```
sudo ssh -nNT -L SOME_PORT:JENKINS_EC_PRIVATE_IP:8080 -i PEM_KEY_PATH_FOR_BASTION ec2-user@BASTION_PUBLIC_IP
(i.e sudo ssh -nNT -L 2290:10.208.225.90:8080 -i ~/.ssh/solvay/shared ec2-user@34.241.195.59)
```
##### note: Bastion's security group needs to whitelist your IP address to ssh into it
3. access `localhost:PORT` (i.e. `localhost:2290` in the above) from a browser to access Jenkins

### Jenkinsfile Anatomy <a name="jenkins_anatomy"></a>

Jenkinsfile executes a Packer docker container:
```
docker run --rm \
      --workdir /${REPO_NAME}/${PACKER_FOLDER_PATH}/${params.AMI_TYPE} \
      --volume ${WORKSPACE}:/${REPO_NAME} \
      --env AWS_ACCESS_KEY_ID=\${AWS_ACCESS_KEY_ID} \
      --env AWS_SECRET_ACCESS_KEY=\${AWS_SECRET_ACCESS_KEY} \
      --env AWS_SESSION_TOKEN=\${AWS_SESSION_TOKEN} \
      hashicorp/packer:light build -var-file=variables.json ${params.AMI_TYPE}.json
```
This command passes AWS credentials to docker container via `docker run --env` option because Packer script gets executed in a docker container not your local machine. 

## 4. How to Test <a name="how_to_test"></a>
Local Testing](#local_testing)
        * [Packer Local Testing](#packer_local_testing)
        * [Ansible Local Testing](#ansible_local_testing)
    * [Remote Testing (on Jenkins)](#remote_testing)

### Local testing <a name="local_testing"></a>

#### Prerequisite <a name="prerequisite"></a>
* AWS access key and secret access key
* Packer (from Hashicorp)
* Vagrant (from Hashicorp)
* Ansible (Python is prerequisite)

#### Packer Local Testing <a name="local_testing"></a>
Given Packer is installed on your compute, AWS profile is configured in `~/.aws/config`, AWS credentials stored in `~/.aws/credentails`, and AWS Profile is set as in `AWS_PROFILE=YOUR_PROFILE`, simply execute
```
packer build \
  -var-file=variables.json \
  ubuntu_jenkins_ami.json
```

Or using a Packer Docker image (Docker needs to be installed), simply execute

```
docker run \
  --env "AWS_ACCESS_KEY_ID=YOUR_KEY" \
  --env "AWS_SECRET_ACCESS_KEY=YOUR_SECRET_KEY" \
  --env AWS_REGION="eu-central-1" \
  --workdir /REPO_NAME/packer/amazon_linux_2_ami \
  --volume $PWD:/image-bakery \
  hashicorp/packer:light build -var-file=variables.json \
  amazon_linux_2_ami.json
```
This command passes AWS credentials to docker container via `docker run --env` option because Packer script gets executed in a docker container not your local machine. 

#### Ansible Local Testing <a name="ansible_local_testing"></a>
To test Ansible locally, use `Vagrant` to spin up a VM on your local environment (VirtualBox needs to be installed in case of Mac).

Modify the below line to specify a Ansible Playbook file you want to test:
```
ansible.playbook = "../rhel_7_ami/playbook.yml"
```
in [Vagrantfile](ansible/test/Vagrantfile)
```
Vagrant.configure(2) do |config|

  config.vm.box = "generic/rhel7"
  #config.vm.box = "centos/7"

  # Disable the new default behavior introduced in Vagrant 1.7, to
  # ensure that all Vagrant machines will use the same SSH key pair.
  # See https://github.com/hashicorp/vagrant/issues/5005
  config.ssh.insert_key = false

  config.vm.provision "ansible" do |ansible|
    ansible.verbose = "v"
    ansible.playbook = "../rhel_7_ami/playbook.yml"
    # ansible.galaxy_roles_path = '../rhel_7_ami/roles'
    # ansible.galaxy_role_file = "../rhel_7_ami/requirements.yml"
  end
end
```

Then simply `vagrant up` to spin up a VM. Ansible Playbook file will be executed against the newly created VM. You can `vagrant ssh` into the machine for debugging as well.


### Remote Testing (on Jenkins) <a name="remote_testing"></a>

Jenkins EC2 instance comes with necessary packages such as Docker, Python, Ansible, etc. All you need to do is to access Jenkins URL ([How to access Jenkins](#how_to_access_jenkins)), navigate to this repo's pipeline and trigger a new build.



