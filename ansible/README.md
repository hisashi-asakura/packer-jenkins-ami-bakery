## Get Started with Ansible to install Jenkins using Ansible Roles

### Local testing
1. `cd test`
2. `vagrant up` to start a VM in Virtual Box (preprequisite)
3. `vagrant provision` and `vagrand destroy` to modify and destry a VM

*Note: 

- `Vagrantfile` looks for a `ansible.playbook` variable to find  `playbook.yml`. You can change this path if you want to test a different playbook.

- `Vagrantfile` also contains a VM image info. You can swap a VM image by setting `config.vm.box` variable.

### Remote testing
One of the ways entails using `packer` to spin up an EC2 instance and have the `Ansible` script run on the instance. To do so, go do `/image-bakery/packer` and run `packer build` command.

Check packer's README instruction for more info.
