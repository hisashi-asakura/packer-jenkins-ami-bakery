pipeline {
    agent none

    environment {
        PACKER_FOLDER_PATH = "packer"
        REPO_NAME = "solvay-image-bakery"
        INSTANCE_TYPE = "m3.large"
        BUILDER_NAME = "packer"
        TAG_NAME = "Packer Builder for"
        TERRAFORM_IMAGE_TAG = "0.11.14"
        SHARED_ACCOUNT_ID = "243274418888"
        IAM_ROLE_NAME = "TerraformBuilder"
        INSTANCE_PROFILE_NAME = "Jenkins"
        // these AMI IDs are in eu-west-1
        UBUNTU_BASE_AMI_ID = "ami-08660f1c6fb6b01e7"
        // RHEL 7.0
        RHEL_BASE_AMI_ID = "ami-8cff51fb"
    }

    parameters {
        string(name: 'GIT_REPO_URL', defaultValue: 'https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/solvay-image-bakery', description: 'CodeCommit repo URL where packer script lives')
        string(name: 'AWS_REGION', defaultValue: 'eu-west-1', description: 'AWS region in which to create an AMI')
        choice(name: 'AMI_TYPE', choices: ['ubuntu_jenkins_ami', 'amazon_linux_2_ami', 'rhel_7_ami', 'win_2016_ami'], description: 'Type of AMI you want to bake')
        choice(name: 'AWS_CRED_ID', choices: ['solvay-iam-user-jenkins'], description: 'IAM credential to use')
        string(name: 'KMS_KEY', defaultValue: 'arn:aws:kms:eu-west-1:243274418888:key/ea5480cb-cb8c-4f54-b7fa-5f29eca7a069', description: 'ID, alias or ARN of the KMS key to use for boot volume encryption')
    }

    stages {
        stage('Clean workspace') {
            agent any 
            steps {
                sh 'sudo rm -rf *'
            }
        }

        stage('Set Packer variables') {
            agent any 
            steps {
                replaceVariables(getLatestBaseAMI(), getSshUsername())
                // need to stash otherwise workspace in next stage won't pick up the changes made to the var file
                stash includes: '**', name: 'source'
            }
        }

        stage('Build AMI') {
            agent any 
            steps {
                // unstash the changes made to the variables.json in the previous stage
                unstash 'source'
                cleanUpDockerContainerCaches()
                buildAMI()
            }
        }
    }
}

def getLatestBaseAMI() {
  def baseAMI

  switch("${params.AMI_TYPE}") {
    case "ubuntu_jenkins_ami":
      baseAMI = "${UBUNTU_BASE_AMI_ID}";
      break;
    case "rhel_7_ami":
      baseAMI = "${RHEL_BASE_AMI_ID}";
      break;
    case "amazon_linux_2_ami":
      baseAMI = sh (
        script: "aws ssm get-parameters --names /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-ebs --region ${params.AWS_REGION} --output json --query Parameters[0].Value --output text",
        returnStdout: true
      ).trim()
      break;
    case "win_2016_ami":
        baseAMI = sh (
          script: "aws ssm get-parameters --names /aws/service/ami-windows-latest/Windows_Server-2016-English-Full-Base --region ${params.AWS_REGION} --output json --query Parameters[0].Value --output text",
          returnStdout: true
        ).trim()
        echo baseAMI
      break;
    default:
      // Ubuntu Server 16.04 LTS (HVM), SSD Volume Type (64-bit x86)
      baseAMI = "${UBUNTU_BASE_AMI_ID}";
  }

  echo baseAMI
  return baseAMI
}

def getSshUsername() {
  def sshUsername

  switch("${params.AMI_TYPE}") {
    case "ubuntu_jenkins_ami":
      sshUsername = "ubuntu";
      break;
    case "win_2016_ami":
      sshUsername = "Administrator";
      break;
    default:
      sshUsername = "ec2-user";
  }

  return sshUsername
}

def cleanUpDockerContainerCaches() {
  sh """
    docker container ls -a
    docker system prune -f
    docker container ls -a
  """
}

def replaceVariables(baseAMI, sshUsername) {
    dir("${PACKER_FOLDER_PATH}/${params.AMI_TYPE}") {
        withCredentials([[
                      $class: 'AmazonWebServicesCredentialsBinding',
                      credentialsId: "${params.AWS_CRED_ID}",
                      accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                      secretKeyVariable: 'AWS_SECRET_ACCESS_KEY'
                  ]]) {
            sh """
                ls

                echo 'fetching Jenkins instance profile temp creds and setting them in Packer variable file...'
                export AWS_ACCESS_KEY_ID=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/${INSTANCE_PROFILE_NAME} | jq -r ".AccessKeyId"`
                export AWS_SECRET_ACCESS_KEY=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/${INSTANCE_PROFILE_NAME} | jq -r ".SecretAccessKey"`
                export AWS_SESSION_TOKEN=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/${INSTANCE_PROFILE_NAME} | jq -r ".Token"`

                pwd
                ls

                echo 'writing temp AWS creds to the packer variable file...'
                sed -i 's|"aws_region.*|"aws_region": "'"${params.AWS_REGION}"'",|
                        s|"base_ami.*|"base_ami": "'"${baseAMI}"'",|
                        s|"instance_type.*|"instance_type": "'"${INSTANCE_TYPE}"'",|
                        s|"name.*|"name": "'"${params.AMI_TYPE}"'",|
                        s|"ssh_username.*|"ssh_username": "'"${sshUsername}"'",|
                        s|"tag_name.*|"tag_name": "'"${TAG_NAME}"' '"${params.AMI_TYPE}"'",|
                        s|"ami_type.*|"ami_type": "'"${params.AMI_TYPE}"'",|
                        s|"kms_key_id.*|"kms_key_id": "'"${params.KMS_KEY}"'",|
                        s|"aws_access_key.*|"aws_access_key": "'"\${AWS_ACCESS_KEY_ID}"'",|
                        s|"aws_secret_key.*|"aws_secret_key": "'"\${AWS_SECRET_ACCESS_KEY}"'",|
                        s|"aws_security_token.*|"aws_security_token": "'"\${AWS_SESSION_TOKEN}"'"|' variables.json

                cat variables.json
            """
        }
    }
}

def buildAMI() {
    sh """
        echo 'resetting envs for temp AWS creds that docker needs...'
        export AWS_ACCESS_KEY_ID=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/${INSTANCE_PROFILE_NAME} | jq -r ".AccessKeyId"`
        export AWS_SECRET_ACCESS_KEY=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/${INSTANCE_PROFILE_NAME} | jq -r ".SecretAccessKey"`
        export AWS_SESSION_TOKEN=`curl http://169.254.169.254/latest/meta-data/iam/security-credentials/${INSTANCE_PROFILE_NAME} | jq -r ".Token"`

        echo 'checking Packer docker version...'
        docker run --rm hashicorp/packer:light version

        echo 'running Packer script in Packer container...'
        docker run --rm \
          --workdir /${REPO_NAME}/${PACKER_FOLDER_PATH}/${params.AMI_TYPE} \
          --volume ${WORKSPACE}:/${REPO_NAME} \
          --env AWS_ACCESS_KEY_ID=\${AWS_ACCESS_KEY_ID} \
          --env AWS_SECRET_ACCESS_KEY=\${AWS_SECRET_ACCESS_KEY} \
          --env AWS_SESSION_TOKEN=\${AWS_SESSION_TOKEN} \
          hashicorp/packer:light build -var-file=variables.json ${params.AMI_TYPE}.json
    """
}