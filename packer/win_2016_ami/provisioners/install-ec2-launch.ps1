# installation doc: https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/ec2launch-download.html
Invoke-WebRequest -Uri https://s3.amazonaws.com/ec2-downloads-windows/EC2Launch/latest/EC2-Windows-Launch.zip -OutFile EC2-Windows-Launch.zip
Invoke-WebRequest -Uri https://s3.amazonaws.com/ec2-downloads-windows/EC2Launch/latest/install.ps1 -OutFile install.ps1
& .\install.ps1
